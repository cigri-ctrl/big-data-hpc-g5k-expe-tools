#!/usr/bin/env python3
"""
This script is running a complete OAR cluster deployment on Grid'5000 to
experiment OAR scheduling policies in real conditions.

It use the development version of an OAR scheduler name Kamelot

It give as an results information about scheduling that was done.
"""
import os
import sys
import json
import time
from shutil import copy
from execo import Process, SshProcess
from execo_engine import Engine, logger, ParamSweeper, sweep
from execo_g5k import oardel
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), '..')))

import expe_tools
from expe_tools import (
    get_resources, deploy_images, configure_SSH, load_experiment_config,
    gather_nodes_information, configure_Hadoop, format_HDFS, start_HDFS,
    stop_HDFS, configure_Spark, create_HDFS_user_home, stop_YARN, start_YARN,
    stop_Spark_history_server, start_Spark_history_server, generate_data,
    push_dataset_to_HDFS, mount_storage5k, configure_OAR,
    activate_OAR_Prologue_Epilogue, add_label_to_nodes
)


script_path = os.path.dirname(os.path.realpath(__file__))


class ReplayBigDataWorkload(Engine):

    def init(self):
        parser = self.options_parser
        parser.add_option('--skip_reconfig',
                          action='store_true',
                          default=False,
                          help='if set and oar_job_id is set, the cluster'
                          'is re-configured')
        parser.add_option('--skip_data_ingestion',
                          action='store_true',
                          default=False,
                          help='if set skip dataset ingestion to HDFS')
        parser.add_option('--force_redeploy',
                          help='can be all, master, slaves')
        parser.add_option('--force_hdfs_format',
                          action='store_true',
                          default=False,
                          help='if set, HDFS format will be forced')
        parser.add_option('--oar_job_id',
                          help="Grid'5000 reservation job ID")
        parser.add_option('--oar_inner_job_id',
                          help="Grid'5000 container to schedule in job ID")

        parser.add_argument('experiment_config',
                            'The config JSON experiment description file')

    def setup_result_dir(self):
        self.result_dir = expe_tools.setup_result_dir()

    def run(self):
        # get CLI paramerters
        oar_job_id = int(self.options.oar_job_id) \
            if self.options.oar_job_id is not None else None
        oar_inner_job_id = int(self.options.oar_inner_job_id) \
            if self.options.oar_inner_job_id is not None else None
        skip_reconfig = self.options.skip_reconfig
        skip_data_ingestion = self.options.skip_data_ingestion
        force_hdfs_format = self.options.force_hdfs_format

        # make the result folder writable for all
        os.chmod(self.result_dir, 0o777)

        # get configuration
        config = load_experiment_config(self.args[0], self.result_dir)
        site = config["grid5000_site"]
        # FIXME resource and job ID are not used and can be wrong if the
        # oar_job_id is set
        resources = config["resources"]
        walltime = str(config["walltime"])
        big_data_timeout = config["big_data_timeout"]

        slave_env = config["kadeploy_slave_env"]
        master_env = config["kadeploy_master_env"]
        workloads = config["workloads"]
        required_resource_nb = config["required_resource_nb"]
        data_dir = config.get("data_dir", None)
        nb_AM_reserved_node = config["nb_AM_reserved_node"]
        nb_BD_reserved_node = config["nb_BD_reserved_node"]
        assert nb_BD_reserved_node >= nb_AM_reserved_node
        logger.info(config)
        # check if workloads exists (Suppose that the same NFS mount point
        # is present on the remote and the local environment
        for workload_files in workloads.values():
            for workload_file in workload_files:
                with open(workload_file):
                    pass
                # copy the workloads files to the results dir
                copy(workload_file, self.result_dir)

        # define the workloads parameters
        logger.info('Workloads: {}'.format(workloads))
        self.parameters = workloads

        # define the iterator over the parameters combinations
        self.sweeper = ParamSweeper(os.path.join(self.result_dir, "sweeps"),
                                    sweep(self.parameters))

        # Due to previous (using -c result_dir) run skip some combination
        logger.info('Skipped parameters:' +
                    '{}'.format(str(self.sweeper.get_skipped())))

        logger.info('Number of parameters combinations {}'.format(
            str(len(self.sweeper.get_remaining()))))
        logger.info('combinations {}'.format(
            str(self.sweeper.get_remaining())))

        options = "--name={}".format(self.run_name)
        if oar_inner_job_id:
            options = "{} -t inner={}".format(options, oar_inner_job_id)
        job_id, nodes = get_resources(resources, walltime, site,
                                      oar_job_id=oar_job_id,
                                      options=options)

        # master is the first node of the reservation others are slaves
        master = nodes[0]
        slaves = nodes[1:required_resource_nb]

        # update the nodes with only the selected ones
        nodes = [master] + slaves

        if len(slaves) + 1 != required_resource_nb:
            raise RuntimeError("Number of available nodes {} is not equals to"
                               " the number of required resources {}".format(
                                   len(slaves) + 1, required_resource_nb))

        # do deployment
        deploy_images(master, master_env, slaves, slave_env,
                      force_redeploy=self.options.force_redeploy,
                      required_resource_nb=required_resource_nb)

        # hdfs home dir
        home_dir = "/user/" + os.getlogin()

        # store node list
        nodes_info = gather_nodes_information(nodes, self.result_dir)

        # Select the nodes to remove from OAR to have dedicated nodes for
        # Hadoop. Also, avoid job killing because of Application master
        # preemption with a dedicated AM partition
        AM_reserved_nodes = slaves[:nb_AM_reserved_node]
        BD_reserved_nodes = slaves[:nb_BD_reserved_node]
        oar_slaves = slaves[nb_BD_reserved_node:]
        logger.info("OAR slaves {}".format(oar_slaves))

        if not skip_reconfig:
            # Configuration
            # Create temporary directory
            tmp_dir = self.result_dir + '/tmp'
            Process('mkdir -p ' + tmp_dir).run()

            # needed for HDFS and Spark setup
            configure_SSH(tmp_dir, master, slaves)

            # Configure RJMS
            configure_OAR(tmp_dir, self.result_dir, master, oar_slaves,
                          nodes_info)

            # enable bebida mechanism
            activate_OAR_Prologue_Epilogue(master, oar_slaves)

            configure_Hadoop(tmp_dir, master, slaves, nodes_info)

            try:
                stop_HDFS(master)
                format_HDFS(master, slaves, force=force_hdfs_format)
            except Exception as e:
                logger.info(e)

            #stop_HDFS(master)
            start_HDFS(master)

            configure_Spark(tmp_dir, master, slaves, nodes_info,
                            enable_labels=True)

            create_HDFS_user_home(master, os.getlogin())

            stop_YARN(master, slaves)
            start_YARN(master, slaves)

            node_label_map = {
                node: "AM_only"
                for node in slaves if node in AM_reserved_nodes}
            node_label_map.update(
                {node: "AM_or_executor"
                 for node in slaves if
                 nodes in BD_reserved_nodes and node not in AM_reserved_nodes}
            )
            node_label_map.update(
                {node: "executor_only"
                 for node in slaves if
                 node not in BD_reserved_nodes}
            )

            add_label_to_nodes(master, node_label_map)
            stop_Spark_history_server(master)
            start_Spark_history_server(master)

        if not skip_data_ingestion:
            # Mount the data directory
            mount_storage5k(nodes, data_dir)

            # get all big data profiles
            bd_profiles = {}
            for workload_file in workloads["spark_workload"]:
                with open(workload_file) as wl:
                    bd_profiles = dict(
                        bd_profiles, **json.load(wl)["profiles"])

            # populate HDFS in parallel (one node per dataset)
            processes = []
            i = 0
            already_done = set()
            for profile in bd_profiles.values():
                dataset_id = (profile["data_size"],
                                  profile["application_type"])
                if dataset_id in already_done:
                    continue
                already_done.add(dataset_id)
                node = BD_reserved_nodes[i % len(BD_reserved_nodes) - 1]
                process = push_dataset_to_HDFS(node,
                                               profile,
                                               data_dir,
                                               home_dir)
                if process is not None:
                    i = i + 1
                    processes.append(process)
            for process in processes:
                process.wait()
                if not process.ok:
                    raise Exception(
                        "Error while populating HDFS: {}".format(
                            process.stdout))

        # Do the replay
        logger.info('Starting to replay')
        while len(self.sweeper.get_remaining()) > 0:
            combi = self.sweeper.get_next()
            logger.info("Combi: {}; Combi ID: {}".format(combi, id(combi)))

            # Be sure that no Big Data jobs are runnning
            cmd = (
                "($HADOOP_HOME/bin/yarn application -list | grep application_ "
                "| cut -f1 | xargs $HADOOP_HOME/bin/yarn application kill ) "
                "|| true"
            )
            SshProcess(cmd, master, shell=True).run()

            # Be sur that no OAR jobs are running
            cmd = (
                'oardel $(oarstat -J | python -c "import sys;import json; '
                'print(\' \'.join([str(job) for job in json.load(sys.stdin)]))'
                '") || true'
            )
            SshProcess(cmd, master, shell=True).run()

            # Clean all previous results
            cmd = '$HADOOP_HOME/bin/hdfs dfs -rm -r -f "result*"'
            SshProcess(cmd, master, shell=True).run()

            # Spark workload replay
            workload_file = os.path.basename(combi['spark_workload'])
            spark_result_dir = "spark_" + str(id(combi)) + "_" + workload_file
            spark_replay = SshProcess(script_path
                                      + "/../run_big_data_workload/spark_replay.py "
                                      + combi['spark_workload'] + " "
                                      + self.result_dir + " "
                                      + spark_result_dir + " > "
                                      + self.result_dir + '/' + spark_result_dir + '.out',
                                      master,
                                      shell=True)

            # HPC workload replay
            workload_file = os.path.basename(combi['hpc_workload'])
            oar_result_dir = "oar_" + str(id(combi)) + "_" + workload_file
            oar_replay = SshProcess(script_path
                                    + "/../run_hpc_workload/oar_replay.py "
                                    + combi['hpc_workload'] + " "
                                    + self.result_dir + " "
                                    + oar_result_dir + " > "
                                    + self.result_dir + '/' + oar_result_dir + '.out',
                                    master,
                                    shell=True)

            logger.info("replaying workload: {}".format(combi))
            # Run both replay
            spark_replay.start()
            oar_replay.run()

            logger.info("OAR replay is finished")

            # This timeout should correspond to at least the longest task
            # execution time in second
            spark_replay.wait(big_data_timeout)
            spark_replay.kill(auto_force_kill_timeout=False)
            data_retrieval_timeout = 120
            time.sleep(data_retrieval_timeout)

            logger.info("Spark replay is finished")

            if oar_replay.ok:
                logger.info("Replay workload OK: {}".format(combi))
                self.sweeper.done(combi)
            else:
                logger.info("Replay workload NOT OK: {}".format(combi))
                self.sweeper.cancel(combi)
                if oar_inner_job_id:
                    oardel([(job_id, site)])
                logger.error("error in the replay: Abort!")
                break

        SshProcess("$HADOOP_HOME/bin/hdfs dfs -copyToLocal "
                   "/user/spark/applicationHistory {}/spark_app_logs".format(self.result_dir),
                   master,
                   shell=True).run()

        logger.info("This job is finished (OAR_JOB_ID: {})".format(job_id))
        logger.info("Result directory: {})".format(self.result_dir))
        if oar_inner_job_id:
            oardel([(job_id, site)])

if __name__ == "__main__":
    engine = ReplayBigDataWorkload()
    expe_tools.script_path = script_path
    engine.start()
