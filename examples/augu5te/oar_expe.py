#!/usr/bin/env python3

""" This script is running a complete OAR cluster deployment on Grid'5000 to experiment OAR scheduling policies in real conditions. 
"""
import os
import sys
from execo import Process, SshProcess
from execo_engine import Engine, logger

sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), '../..')))

import expe_tools
from expe_tools import (get_resources, deploy_images, configure_OAR,
                        load_experiment_config, gather_nodes_information)

script_path = os.path.dirname(os.path.realpath(__file__))

is_a_test = False

class ReplayOarWorkload(Engine):
    def __init__(self):
        super(ReplayOarWorkload, self).__init__()
        parser = self.args_parser
        parser.add_argument('--oar_job_id',
                            help="Grid'5000 reservation job ID")
        
        parser.add_argument('--oar_inner_job_id',
                            help="Grid'5000 container to schedule in job ID")
        parser.add_argument('--force_reconfig',
                            dest='force_reconfig',
                            action='store_true',
                            default=False,
                            help='if set and oar_job_id is set, the cluster'
                            'is re-configured')
        parser.add_argument('--force_redeploy',
                            help='can be all, master, slaves')

        parser.add_argument('experiment_config',
                            help='The config YAML experiment description file')

    def setup_result_dir(self):
        self.result_dir = expe_tools.setup_result_dir(is_a_test)

    def run(self):
        # get CLI paramerters
        oar_job_id = int(self.args.oar_job_id) \
            if self.args.oar_job_id is not None else None
        oar_inner_job_id = int(self.options.oar_inner_job_id) \
            if self.args.oar_inner_job_id is not None else None
        force_reconfig = self.args.force_reconfig
        
        # make the result folder writable for all
        os.chmod(self.result_dir, 0o777)
        
        # get configuration
        config = load_experiment_config(self.args.experiment_config, self.result_dir)
        site = config['grid5000_site']

        walltime = str(config['walltime'])

        additional_nodes = config['additional_nodes'] if 'additional_nodes' in config else 0   
        properties = config['properties'] if 'properties' in config else None
        nb_storage_servers = config['nb_storage_servers'] if 'nb_storage_servers' in config else 0

        oar_node_env = config['oar_node_env']
        oar_server_env = config['oar_server_env']            

        nb_servers = 1 + nb_storage_servers
        nb_oar_nodes = config['nb_nodes']
        nb_nodes = additional_nodes + nb_servers + nb_oar_nodes
        nb_resources_per_node = config['nb_resources_per_node']
        
        options = '--name={}'.format(self.run_name)
        if oar_inner_job_id:
            options = '{} -t inner={}'.format(options, oar_inner_job_id)
        if properties:
            options = options + ' ' + properties
        job_id, nodes = get_resources('nodes={}'.format(nb_nodes), walltime, site,
                                      oar_job_id=oar_job_id,
                                      options=options)
        logger.info('Nb Asked Nodes: {}'.format(nb_nodes))
        # do deployment
        oar_servers = nodes[0:nb_servers]
        oar_nodes = nodes[nb_servers:nb_servers + nb_oar_nodes]
        
        deploy_images(oar_servers, oar_server_env, oar_nodes, oar_node_env,
                      force_redeploy=self.args.force_redeploy,
                      required_resource_nb=nb_nodes)
        
        # Store node list
        nodes_info = gather_nodes_information(nodes, self.result_dir)
        
        # Configuration
        # Create temporary directory
        tmp_dir = self.result_dir + '/tmp'
        Process('mkdir -p ' + tmp_dir).run()
        
        configure_OAR(tmp_dir, self.result_dir, oar_servers[0],
                      oar_nodes, nodes_info, disable_cigri_api=True,
                      nb_res_per_node=nb_resources_per_node)

if __name__ == '__main__':
    engine = ReplayOarWorkload()
    expe_tools.script_path = script_path
    engine.start()
